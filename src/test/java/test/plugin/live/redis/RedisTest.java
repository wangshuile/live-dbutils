package test.plugin.live.redis;

import org.junit.Before;
import org.junit.Test;

import com.jfinal.plugin.redis.RedisPlugin;

import plugin.live.redis.RedisObjByte;

public class RedisTest {

	@Test
	public void save() {
		RedisObjByte<RedisEntity> redis = new RedisObjByte<RedisEntity>("cacheKey_redisTest");
		// 保存
		RedisEntity entity = new RedisEntity();
		entity.setName("测试");
		redis.save(entity);
		// 获取
		entity = redis.getObj();
	}

	@Before
	public void init() {
		new RedisPlugin("", "ip", 6379, "hMhp3H5BnA").start();
	}
}
