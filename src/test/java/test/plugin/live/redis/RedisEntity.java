package test.plugin.live.redis;

import java.io.Serializable;

import lombok.AllArgsConstructor;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RedisEntity implements Serializable{
	private static final long serialVersionUID = 8206409519526715508L;
	
	private String name;
	private int age;
	
}
