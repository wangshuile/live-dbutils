package test.plugin.live.mongodb;

import lombok.Data;
import plugin.live.mongodb.MongoBeanMy;

@Data
public class TestMongB extends MongoBeanMy{
	private String name;
	private Integer age;
	private Boolean flag;
}
