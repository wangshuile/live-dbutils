package test.plugin.live.mongodb;

import java.util.List;

import org.junit.Before;

import com.alibaba.fastjson.JSONObject;
import com.cybermkd.mongo.kit.MongoQuery;
import com.cybermkd.mongo.plugin.MongoJFinalPlugin;

import plugin.live.mongodb.MongoOom;

public class MongoOomTest {
	
	public List<TestMongB> findList() {
		TestMongB obj = new TestMongB();
		obj.setAge(9);//条件1
//		obj.setName("晓绿");//条件2
		MongoOom<TestMongB> oom = new MongoOom<TestMongB>(TestMongB.class);
		List<TestMongB> find = oom.find(obj);//对象查询
		List<JSONObject> find2 = new MongoQuery().use("TestMongB").eq("age", 9).eq("name", "名字1").find();//需要匹配条件
		return find;
	}
	
//	@Test
	public void find() {
		List<TestMongB> find = this.findList();
		MongoOom<TestMongB> oom = new MongoOom<TestMongB>(TestMongB.class);
		
		find.forEach(t->{
			System.out.println(t);
			System.out.println("id:"+t.getId());
			TestMongB findById = oom.findById(t.getId());
			System.out.println("id获取对象:"+findById);
		});
	}
	
//	@Test
	public void update() {
		List<TestMongB> find = this.findList();
		MongoOom<TestMongB> oom = new MongoOom<TestMongB>(TestMongB.class);
		find.forEach(t->{
			//修改
			t.setName(t.getName() + "修改");
			System.out.println(oom.updateById(t));
		});
	}
	
//	@Test
	//删除匹配条件
	public void deleteMore() {
		TestMongB obj = new TestMongB();
		obj.setAge(9);
		MongoOom<TestMongB> oom = new MongoOom<TestMongB>(TestMongB.class);
		System.out.println(oom.del(obj));
	}
	
//	@Test
	public void delOne() {
		MongoOom<TestMongB> oom = new MongoOom<TestMongB>(TestMongB.class);
		System.out.println(oom.del("5ab5d1eb0a3241071c1e9de9"));
	}
	
//	@Test
	public void save() {
		
		TestMongB obj = new TestMongB();
		obj.setAge(9);
		obj.setFlag(false);
		obj.setName("晓明");
		boolean save2 = new MongoOom<TestMongB>(TestMongB.class).save(obj);
		
		boolean save = new MongoQuery().use("TestMongB").set(obj).save();
	}
	
	@Before
	public void init() {
		MongoJFinalPlugin jFinalPlugin = new MongoJFinalPlugin();
		jFinalPlugin.add("ip地址", 27017);
		jFinalPlugin.setDatabase("admin");// 这个要放在用户名/密码前面
		jFinalPlugin.auth("", "");
		jFinalPlugin.start();
	}
}
