package plugin.live.mongodb;

import com.alibaba.fastjson.annotation.JSONField;

import lombok.Data;

@Data
public class MongoBeanMy {
	private String id;
	
	@JSONField(name = "_id")
	public String getId() {
		return id;
	}
}
