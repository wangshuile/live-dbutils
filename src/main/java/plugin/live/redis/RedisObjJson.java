package plugin.live.redis;

import com.jfinal.plugin.redis.Cache;
import com.jfinal.plugin.redis.Redis;

import lombok.Data;

@Data
public class RedisObjJson<T> {
	private Cache use = Redis.use();
	private String cacheName;

	public RedisObjJson(String cacheName) {
		this.cacheName = cacheName;
	}

	public void setObj(T t) {
		byte[] byte1 = this.getByte(t);
		use.set(cacheName, byte1);
		// use.setex(t.getClass().getSimpleName(),5, this.getByte(t));
	}

	public void setObj(T t, int timeOutSeconds) {
		use.setex(cacheName, timeOutSeconds, this.getByte(t));
	}

	public T getObj() {
		// String simpleName = t.getClass().getSimpleName();
		return this.getObj((byte[]) use.get(cacheName));
	}
	
	public void delete(){
		use.del(cacheName);
	}

	private byte[] getByte(T t) {
		return SerializeUtil.serialize(t);
	}

	@SuppressWarnings("unchecked")
	private T getObj(byte[] bytes) {
		return (T) SerializeUtil.unserialize(bytes);
	}

}
