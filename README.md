# live-dbutils

项目介绍：
-------------------------------------

1.live-dbutils是Mongodb Plugin插件的二次封装,之前使用mysql比较喜欢oom操作,最近接触mongodb发现查询都是键值对进行操作的<br>
相关的Mongodb Pluginc插件可以访问https://t-baby.gitbooks.io/mongodb-plugin/content/<br>
本程序不是为了替代原来插件,只是另外提供一些简便封装操作,可以跟原来插件互补进行操作!!!! <br>
2.新增redis缓存对象

联系方式: http://qiongbi.chenwenxi.cc/

Quick Start
-------------------------------------

一个栗子：

### 测试对象


```java
package test.oom.mongo;

import lombok.Data;
import plugin.db.oom.dao.mongodb.MongoBeanMy;

@Data
//MongoBeanMy里面对_id进行转化为id
public class TestMongB extends MongoBeanMy{
	private String name;
	private Integer age;
	private Boolean flag;
}
```


### 保存对象对比
```java
public void save() {
    TestMongB obj = new TestMongB();
    obj.setAge(9);
    obj.setFlag(false);
    obj.setName("晓明");
    //自动生成表名
    boolean save1 = new MongoOom<TestMongB>(TestMongB.class).save(obj);
    //需要指定表名
	boolean save2 = new MongoQuery().use("TestMongB").set(obj).save();
}
```

### 对象化查询
```java
public List<TestMongB> findList() {
    TestMongB obj = new TestMongB();
    obj.setAge(9);//条件1
//		obj.setName("晓绿");//条件2
    MongoOom<TestMongB> oom = new MongoOom<TestMongB>(TestMongB.class);
    List<TestMongB> find = oom.find(obj);//对象查询
    List<JSONObject> find2 = new MongoQuery().use("TestMongB").eq("age", 9).eq("name", "名字1").find();//需要匹配条件
    return find;
}
```

其他oom操作请参考 MongoOomTest.java 测试类

### Redis对象操作
```java
@Test
public void save() {
    RedisObjByte<RedisEntity> redis = new RedisObjByte<RedisEntity>("cacheKey_redisTest");
    // 保存
    RedisEntity entity = new RedisEntity();
    entity.setName("测试");
    redis.save(entity);
    // 获取
    entity = redis.getObj();
}
```
